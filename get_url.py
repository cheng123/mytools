from sgmllib  import SGMLParser
import threading
import time
import urllib2
import StringIO
import gzip
import string
import os


class Base(SGMLParser):
    def reset(self):
        self.urls=[]
        self.img=[]
        SGMLParser.reset(self)
    def start_a(self,attrs):
        href=[v for k,v in attrs if k=='href']
        if href:
            self.urls.extend(href)
    def start_img(self,attrs):
        img=[v for k,v in attrs if k=='img']
        if img:
            self.img.extend(img)

f=urllib2.urlopen('http://www.baidu.com').read()
parser=Base()
parser.feed(f)


fi=open('url.txt','a+')
for i  in parser.urls:
    fi.write(i)
    fi.write('\n')
fi.close()
